package org.mat.framework.apm.skywalking.config;

import org.mat.framework.apm.skywalking.context.SkywalkingTraceIdAdaptor;
import org.mat.framework.core.context.TraceIdAdaptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: SkywalkingTraceIdAdaptorAutoConfiguration
 * @Date: 2021/8/18
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
@Configuration
public class SkywalkingTraceIdAdaptorAutoConfiguration {

    @Bean
    public TraceIdAdaptor traceIdAdaptor() {
        return new SkywalkingTraceIdAdaptor();
    }

}
