package org.mat.framework.apm.skywalking.context;

import org.mat.framework.core.context.TraceIdAdaptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.TraceContext;
import org.slf4j.MDC;

/**
 * @ClassName: SkywalkingTraceIdGenerator
 * @Date: 2021/8/18
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
@Slf4j
public class SkywalkingTraceIdAdaptor implements TraceIdAdaptor {

    @Override
    public String getTraceId() {
        String traceId = TraceContext.traceId();
        log.info("skywalking tid = {}", MDC.get("tid"));
        log.debug("从skywalking获取traceId : {}", traceId);
        return traceId;
    }

}
