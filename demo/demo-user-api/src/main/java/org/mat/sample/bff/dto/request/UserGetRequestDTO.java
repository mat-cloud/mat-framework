package org.mat.sample.bff.dto.request;


import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>Title: UserGetRequestDTO</p>
 * <p>Date: 2018/10/23 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@Data
@Builder
public class UserGetRequestDTO {

    /**
     * 用户统一标识
     */
    @NotNull(message = "统一用户标识不能为空")
    private String unionId;

}
