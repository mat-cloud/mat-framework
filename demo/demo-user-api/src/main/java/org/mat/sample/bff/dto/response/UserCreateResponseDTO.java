package org.mat.sample.bff.dto.response;


import lombok.Data;

@Data
public class UserCreateResponseDTO {

    /**
     * 用户统一标识
     */
    private String unionId;

}
