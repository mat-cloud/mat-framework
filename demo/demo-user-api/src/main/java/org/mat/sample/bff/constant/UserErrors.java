package org.mat.sample.bff.constant;

import org.mat.framework.lang.exception.ErrorInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: UserErrors</p>
 * <p>Date: 2018/10/24 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public enum UserErrors implements ErrorInfo {

    UPDATE_WITH_NONE_VALID_FIELD("10021", "更新用户，没有传入有效的待更新字段。"),

    USER_NOT_FOUND_ERROR("10015", "用户不存在。"),

    QUERY_SINGLE_MOBILE_PHONE_ILLEGAL("10016", "查询单个用户，手机号码非法。"),

    QUERY_SINGLE_MOBILE_PHONE_IS_NULL("10014", "查询单个用户，手机号码为空。"),

    QUERY_SINGLE_USER_ID_IS_NULL("10013", "查询单个用户，用户ID为空。"),

    QUERY_SINGLE_MERCHANT_CODE_IS_NULL("10012", "查询单个用户，商户编码为空。"),

    QUERY_SINGLE_TYPE_ERROR("10011", "查询单个用户，查询类型错误。"),

    CREATE_USER_ALREADY_EXISTS("10001", "创建用户失败，用户已存在。");

    /**
     * 状态码
     */
    private int status;

    /**
     * 编码
     */
    private String code;
    /**
     * 消息
     */
    private String msg;

    /**
     * 构造函数
     *
     * @param code
     * @param msg
     */
    UserErrors(String code, String msg) {
        this.status = 200;
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }


    private static Map<String, UserErrors> valueMap = new HashMap<>();

    static {
        UserErrors[] values = UserErrors.values();
        for (UserErrors anEnum : values) {
            valueMap.put(anEnum.getCode(), anEnum);
        }
    }
}
