package org.mat.sample.bff.api;

import org.mat.framework.lang.dto.MatApiResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>Title: LoopbackApiService </p>
 * <p>Date: 2020/3/17 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@FeignClient(name = "demo-user-service")
public interface LoopbackApiService {

    @GetMapping("/v1/demo/user/loopback")
    MatApiResponse<String> doLoopback(@RequestParam("str") String str);
}
