package org.mat.sample.bff.dto.request;


import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * <p>Title: UserUpdateByUserIdRequestDTO</p>
 * <p>Date: 2018/10/24 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@Data
@Builder
public class UserUpdateByUserIdRequestDTO {

    /**
     * 商户ID
     */
    @NotBlank(message = "商户编码必填")
    private String merchantCode;

    /**
     * 用户编号
     */
    @NotBlank(message = "用户ID必填")
    private String userId;
    /**
     * 手机号
     */
    private String mobilePhone;

    /**
     * 状态
     * <p>
     * 0-注销 1-正常 2-停用
     */
    private Integer status;

}
