package org.mat.sample.bff.dto.response;


import lombok.Data;

import java.io.Serializable;

/**
 * <p>Title: UserDetailDTO</p>
 * <p>Date: 2018/10/23 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@Data
public class UserDetailDTO implements Serializable{

    /**
     * 用户统一标识
     */
    private String unionId;

    /**
     * 商户ID
     */
    private String merchantCode;

    /**
     * 用户编号
     */
    private String userId;

    /**
     * 手机号
     */
    private String mobilePhone;

    /**
     * 注册城市编号
     */
    private Integer regCityId;

    /**
     * 注册时间,格式：unixtime时间戳
     */
    private Long regTime;

    /**
     * 状态
     * <p>
     * 0-注销 1-正常 2-停用
     */
    private Integer status;

}
