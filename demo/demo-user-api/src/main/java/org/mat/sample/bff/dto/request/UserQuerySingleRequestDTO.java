package org.mat.sample.bff.dto.request;


import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * <p>Title: UserQuerySingleRequestDTO</p>
 * <p>Date: 2018/10/23 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@Data
@Builder
public class UserQuerySingleRequestDTO {

    /**
     * 查询类型(1: 按用户查询-商户编号和用户编号必填，2：按手机号查询-商户编号和手机号必填)
     */
    @NotNull(message = "查询类型不能为空")
    private Integer queryType;

    /**
     * 商户ID
     */
    @NotBlank(message = "商户编码必填")
    private String merchantCode;

    /**
     * 用户编号
     */
    private String userId;

    /**
     * 手机号
     */
    private String mobilePhone;

}
