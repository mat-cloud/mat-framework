package org.mat.sample.bff.dto.request;


import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>Title: UserUpdateRequestDTO</p>
 * <p>Date: 2018/10/23 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@Data
@Builder
public class UserUpdateRequestDTO {

    /**
     * 用户统一标识
     */
    @NotNull(message = "用户统一标识必填")
    private String unionId;

    /**
     * 手机号
     */
    private String mobilePhone;

    /**
     * 状态
     * <p>
     * 0-注销 1-正常 2-停用
     */
    private Integer status;

}
