package org.mat.sample.bff.dto.request;


import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Builder
public class UserCreateRequestDTO {

    /**
     * 商户ID
     */
    @NotNull(message = "商户编码必填")
    @NotBlank(message = "商户编码必填")
    private String merchantCode;

    /**
     * 用户编号
     */
    @NotNull(message = "用户ID必填")
    @NotBlank(message = "用户ID必填")
    private String userId;

    /**
     * 手机号
     */
    @NotNull(message = "手机号不能为空")
    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^[0-9]*$", message = "手机号必须是数字")
    private String mobilePhone;

    /**
     * 注册城市编号
     */
    @NotNull(message = "城市编号不能为空")
    private Integer regCityId;

    /**
     * 注册时间,格式：unixtime时间戳
     */
    private Long regTime;
}
