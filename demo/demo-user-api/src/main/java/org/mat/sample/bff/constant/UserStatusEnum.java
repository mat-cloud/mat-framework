package org.mat.sample.bff.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: UserStatusEnum</p>
 * <p>Date: 2018/10/23 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public enum UserStatusEnum {

    /**
     * 注销
     */
    CLOSED(0),

    /**
     * 正常
     */
    ENABLED(1),

    /**
     * 停用
     */
    DISABLED(2);

    private Integer value;

    public Integer getValue() {
        return value;
    }

    UserStatusEnum(Integer value) {
        this.value = value;
    }


    private static Map<Integer, UserStatusEnum> valueMap = new HashMap<>();

    static {
        UserStatusEnum[] values = UserStatusEnum.values();
        for (UserStatusEnum anEnum : values) {
            valueMap.put(anEnum.getValue(), anEnum);
        }
    }

    public static UserStatusEnum withValue(Integer queryType) {
        return valueMap.get(queryType);
    }
}
