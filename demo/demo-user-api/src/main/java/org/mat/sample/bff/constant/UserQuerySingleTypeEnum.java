package org.mat.sample.bff.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: UserStatusEnum</p>
 * <p>Date: 2018/10/23 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public enum UserQuerySingleTypeEnum {
    /**
     * 按用户查询-商户编号和用户编号必填
     */
    BY_USER_ID(1),

    /**
     * 按手机号查询-商户编号和手机号必填
     */
    BY_MOBILE_PHONE(2);


    private Integer value;

    public Integer getValue() {
        return value;
    }

    UserQuerySingleTypeEnum(Integer value) {
        this.value = value;
    }

    private static Map<Integer, UserQuerySingleTypeEnum> valueMap = new HashMap<>();

    static {
        UserQuerySingleTypeEnum[] values = UserQuerySingleTypeEnum.values();
        for (UserQuerySingleTypeEnum anEnum : values) {
            valueMap.put(anEnum.getValue(), anEnum);
        }
    }

    public static UserQuerySingleTypeEnum withValue(Integer queryType) {
        return valueMap.get(queryType);
    }
}
