package org.mat.sample.bff.api;

import org.mat.framework.lang.dto.MatApiResponse;
import org.mat.sample.bff.dto.request.*;
import org.mat.sample.bff.dto.response.UserCreateResponseDTO;
import org.mat.sample.bff.dto.response.UserDetailDTO;
import org.mat.sample.bff.fallback.UserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * <p>Title: UserApiService</p>
 * <p>Date: 2019-06-28 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@FeignClient(name = "usample-bff-service-user", fallbackFactory = UserFallbackFactory.class)
public interface UserApiService {


    /**
     * 创建统一的用户数据
     *
     * @param userCreateRequestDTO {@link UserCreateRequestDTO}
     * @return
     * @since V1.0
     */
    @PostMapping(value = "/v1/bpcs/user/create", consumes = "application/json")
    MatApiResponse<UserCreateResponseDTO> create(@RequestBody UserCreateRequestDTO userCreateRequestDTO);

    /**
     * 获取用户数据
     *
     * @param userGetRequestDTO {@link UserDetailDTO}
     * @return
     * @since V1.0
     */
    @PostMapping(value = "/v1/bpcs/user/get", consumes = "application/json")
    MatApiResponse<UserDetailDTO> get(@RequestBody UserGetRequestDTO userGetRequestDTO);


    /**
     * 根据查询条件获取单个用户信息，返回最新记录
     *
     * @param userQuerySingleRequestDTO {@link UserQuerySingleRequestDTO}
     * @return
     * @since V1.0
     */
    @PostMapping(value = "/v1/bpcs/user/query_single", consumes = "application/json")
    MatApiResponse<UserDetailDTO> querySingle(@RequestBody UserQuerySingleRequestDTO userQuerySingleRequestDTO);


    /**
     * 更新单个用户状态
     *
     * @param userUpdateRequestDTO {@link UserUpdateRequestDTO}
     * @return
     * @since V1.0
     */
    @PostMapping(value = "/v1/bpcs/user/update", consumes = "application/json")
    MatApiResponse update(@RequestBody UserUpdateRequestDTO userUpdateRequestDTO);


    /**
     * 根据merchantCode、userId更新单个用户状态
     *
     * @param userUpdateByUserIdRequestDTO {@link UserUpdateByUserIdRequestDTO}
     * @return
     * @since V1.0
     */
    @PostMapping(value = "/v1/bpcs/user/update_by_userid", consumes = "application/json")
    MatApiResponse updateByUserId(@RequestBody UserUpdateByUserIdRequestDTO userUpdateByUserIdRequestDTO);

}
