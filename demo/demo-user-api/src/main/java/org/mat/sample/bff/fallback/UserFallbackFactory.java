package org.mat.sample.bff.fallback;

import feign.hystrix.FallbackFactory;
import org.mat.framework.lang.dto.MatApiResponse;
import org.mat.sample.bff.api.UserApiService;
import org.mat.sample.bff.dto.request.*;
import org.mat.sample.bff.dto.response.UserCreateResponseDTO;
import org.mat.sample.bff.dto.response.UserDetailDTO;

public class UserFallbackFactory implements FallbackFactory<UserApiService> {
    @Override
    public UserApiService create(Throwable cause) {
        return new UserApiService() {
            @Override
            public MatApiResponse<UserCreateResponseDTO> create(UserCreateRequestDTO userCreateRequestDTO) {
                return MatApiResponse.fail(
                        "9999",
                        "fallback cause = " + cause.getMessage());
            }

            @Override
            public MatApiResponse<UserDetailDTO> get(UserGetRequestDTO userGetRequestDTO) {
                return MatApiResponse.fail(
                        "9999",
                        "fallback cause = " + cause.getMessage());
            }

            @Override
            public MatApiResponse<UserDetailDTO> querySingle(UserQuerySingleRequestDTO userQuerySingleRequestDTO) {
                return MatApiResponse.fail(
                        "9999",
                        "fallback cause = " + cause.getMessage());
            }

            @Override
            public MatApiResponse update(UserUpdateRequestDTO userUpdateRequestDTO) {
                return MatApiResponse.fail(
                        "9999",
                        "fallback cause = " + cause.getMessage());
            }

            @Override
            public MatApiResponse updateByUserId(UserUpdateByUserIdRequestDTO userUpdateByUserIdRequestDTO) {
                return MatApiResponse.fail(
                        "9999",
                        "fallback cause = " + cause.getMessage());
            }
        };
    }
}
