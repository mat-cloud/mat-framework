package org.mat.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients("org.mat.sample")
public class DemoPortalWebApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(DemoPortalWebApplication.class, args);
    }

}
