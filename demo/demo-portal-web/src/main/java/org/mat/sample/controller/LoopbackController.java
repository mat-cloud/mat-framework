package org.mat.sample.controller;

import org.mat.framework.core.exception.AssertUtils;
import org.mat.framework.lang.constant.DefaultErrors;
import org.mat.framework.lang.dto.MatApiResponse;
import org.mat.framework.logging.operation.api.ApiLog;
import org.mat.framework.logging.operation.api.OperateEnum;
import org.mat.sample.bff.api.LoopbackApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Api(value = "loopback", tags = {"环回测试接口"})
public class LoopbackController {

    @Autowired
    private LoopbackApiService loopbackApiService;


    @ApiOperation(value = "测试")
    @GetMapping("/v1/demo/user/loopback")
    @ApiLog(operateType = OperateEnum.QUERY,operateModule = "测试模块")
    public MatApiResponse<String> doLoopback(@RequestParam("str") String str) {
        // 模拟抛出异常
        if ("testError".equals(str)) {
            AssertUtils.assertTrue(false, DefaultErrors.INVALID_PARAM);
        }

        if ("testError2".equals(str)) {
            AssertUtils.assertTrue(false, DefaultErrors.INVALID_PARAM_WITH_PLACEHOLDER, str);
        }

        str += " <== demo-portal-web";

        MatApiResponse<String> stringMatApiResponse = loopbackApiService.doLoopback(str);

        return MatApiResponse.success(stringMatApiResponse.getData());
    }

}
