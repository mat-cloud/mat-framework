DROP TABLE IF EXISTS `assigns_orders`;
CREATE TABLE `assigns_orders` (
  `car_orders_id` varchar(64) NOT NULL,
  `customer_name` varchar(128) DEFAULT NULL,
  `car_type_version` varchar(32) DEFAULT NULL,
  `payment_plan` int(11) DEFAULT NULL,
  `orders_price` bigint(20) DEFAULT NULL,
  `balance` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`car_orders_id`)
);
