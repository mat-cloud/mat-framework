package org.mat.sample.dao;

import org.mat.sample.domain.AssignsOrders;

import java.util.List;

public interface AssignsOrdersMapper {
    int deleteByPrimaryKey(String carOrdersId);

    int insert(AssignsOrders record);

    AssignsOrders selectByPrimaryKey(String carOrdersId);

    List<AssignsOrders> selectAll();

    int updateByPrimaryKey(AssignsOrders record);
}