package org.mat.sample.service;

import org.mat.sample.domain.AssignsOrders;

import java.util.List;

public interface AssignsOrdersService {
    List<AssignsOrders> selectAll();

    void testTraceId();
}
