package org.mat.sample.feign;

import org.mat.framework.lang.dto.MatApiResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "demo-portal-web")
public interface LoopbackFeignService {

    /**
     * sap 银行流水同步
     */
    @GetMapping("/v1/demo/user/loopback")
    MatApiResponse<String> doLoopback(@RequestHeader("") String s, @RequestParam("str") String str);
}