package org.mat.sample.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.mat.framework.lang.dto.MatApiResponse;
import org.mat.sample.dao.AssignsOrdersMapper;
import org.mat.sample.domain.AssignsOrders;
import org.mat.sample.domain.ProjectContext;
import org.mat.sample.feign.LoopbackFeignService;
import org.mat.sample.service.AssignsOrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

@Service
@Slf4j
public class AssignsOrdersImpl implements AssignsOrdersService {

    @Resource
    private AssignsOrdersMapper assignsOrdersMapper;

    @Resource
    private LoopbackFeignService loopbackFeignService;

    @Override
    public List<AssignsOrders> selectAll() {
//        try {
//            Thread.sleep(2000L);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
           return assignsOrdersMapper.selectAll();
    }

    @Override
    public void testTraceId() {
        log.info("当前线程ID：" + Thread.currentThread().getId() + "当前线程Name" + Thread.currentThread().getName());
        String traceId = RequestContextHolder.currentRequestAttributes().getAttribute("context", SCOPE_REQUEST).toString();
        log.info("context："+ traceId);
        ProjectContext projectContext = ProjectContext.getContext();
        projectContext.setTraceId(traceId);
        MatApiResponse<String> stringMatApiResponse = loopbackFeignService.doLoopback(JSONObject.toJSONString(projectContext),"111");
    }

    /**
     * @param 熔断则掉该方法
     * @return
     */
    public void getError(Throwable e) {
        if (e != null){
            log.error(e.getMessage());
        }
    }
}
