package org.mat.sample.config;

import org.mat.sample.constant.LogCollectionConstants;
import org.slf4j.MDC;

/**
 * 2 * @Author: lvdebo
 * 3 * @Date: 2021/8/16 3:10 下午
 * 4
 */
public class LogFeignInterceptorConfig {
    public String getTraceId() {
        return MDC.get(LogCollectionConstants.traceId);
    }
}
