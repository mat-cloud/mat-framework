package org.mat.sample;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@MapperScan("com.sc.**.dao")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.sc.*"})
@SpringBootApplication
@ServletComponentScan
public class DemoUserServiceApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(DemoUserServiceApplication.class, args);
    }
    
}
