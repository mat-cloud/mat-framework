package org.mat.sample.controller;

import org.mat.framework.core.exception.AssertUtils;
import org.mat.framework.lang.constant.DefaultErrors;
import org.mat.framework.lang.dto.MatApiResponse;
import org.mat.sample.bff.api.LoopbackApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 环回测试工具包
 * 用于测试框架各类功能
 *
 * @module 测试模块1
 */
@Slf4j
@RestController
@Api(value = "loopback", tags = {"环回测试接口"})
public class LoopbackController implements LoopbackApiService {

    /**
     * 环回测试-输入字符串原样返回
     * 获取str入参，打印、根据入参内容模拟异常、返回入参
     *
     * @param str 输入的字符串
     * @return 返回标准响应结构，data中为实际业务处理结果
     */
    @Override
    @ApiOperation(value = "环回测试-输入字符串原样返回")
    @GetMapping("/v1/demo/user/loopback")
    public MatApiResponse<String> doLoopback(String str) {
        MatApiResponse<String> ret = MatApiResponse.success(str);
        log.info(str);

        // 模拟抛出异常
        if ("testError".equals(str)) {
            AssertUtils.assertTrue(false, DefaultErrors.INVALID_PARAM);
        }

        if ("testError2".equals(str)) {
            AssertUtils.assertTrue(false, DefaultErrors.INVALID_PARAM_WITH_PLACEHOLDER, str);
        }

        str += " <== demo-user-service";
        ret.setData(str);
        return ret;
    }


}
