package org.mat.sample.domain;

import lombok.Data;

@Data
public class AssignsOrders {
    private String carOrdersId;

    private String customerName;

    private String carTypeVersion;

    private Integer paymentPlan;

    private Long ordersPrice;

    private Long balance;
}