package org.mat.sample.controller;


import com.alibaba.fastjson.JSON;
import org.mat.framework.core.exception.AssertUtils;
import org.mat.framework.lang.constant.DefaultErrors;
import org.mat.framework.lang.dto.MatApiResponse;
import org.mat.sample.domain.AssignsOrders;
import org.mat.sample.service.AssignsOrdersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController
@Api(value = "assignOrders", tags = {"分配订单接口"})
public class AssignOrdersController {

    @Resource
    private AssignsOrdersService assignsOrdersService;

    @ApiOperation(value = "获取所有待分配订单信息")
    @GetMapping("/v1/mat/orders/assign/all")
    public MatApiResponse<String> selectAll() {

        log.info("");//打印请求参数

        List<AssignsOrders> list = null;
        MatApiResponse<String> ret = null;
        try {
            list = assignsOrdersService.selectAll();//访问业务逻辑
            String ordersJson = JSON.toJSONString(list);//转json格式
            ret = MatApiResponse.success("执行成功");
            ret.setData(ordersJson);//封装返回结果
        } catch (Exception e) {
            log.error("执行异常", e);
            AssertUtils.assertTrue(false, DefaultErrors.DEFAULT);
            ret = MatApiResponse.fail(DefaultErrors.DEFAULT.getCode(), DefaultErrors.DEFAULT.getMsg());
        }

        return ret;
    }

}
