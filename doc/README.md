# mat-framework

## 介绍
```
mat-framework是一个服务化开发框架。
框架基于springcloud技术栈开发，并融合实际业务系统开发和服务治理过程的常见需求。
```


## 软件架构
#### 设计原则
- 微核
- 统一依赖
- 分层演进

#### 组件规划

|模块|定位|功能|备注|
|---|---|---|---|
|mat-dependencies|依赖版本定义|---|---|
|mat-kernel-core|核心开发支持|1、模板代码抽象<br>2、上下文支持<br>3、系统日志|---|
|mat-kernel-lang|语言扩展|1、APIResult<br>2、通用注解<br>3、通用异常|这个包是可以对外开放的|
|mat-kernel-util|基础工具|1、加解密<br>2、文件工具<br>3、Http工具|业务相关的工具不会放在这里|
|mat-kernel-web|web扩展|1、异常拦截<br>2、通用controller|---|
|mat-kernel-web-auth|权限管理|1、权限过滤Filter<br>2、黑白名单支持<br>3、自动装配|---|

#### 路线图
- [X] 框架核心
    - [X] 基础工具
    - [X] 语言扩展
    - [X] 核心开发支持 
- [X] web
    - [X] 异常拦截
    - [ ] 通用controller
- [ ] 服务化
    - [ ] 链路跟踪
    - [ ] 灰度
    - [ ] 流控
    - [ ] 熔断&降级
- [X] 日志
    - [X] logging
    - [ ] logging-kafka
    - [ ] logging-db
- [ ] 单点登录
- [ ] 数据访问
    - [ ] mysql
    - [ ] redis
    - [ ] rocketmq
    - [ ] elasticsearch
    - [ ] ...
- [ ] Maven扩展
    - [ ] Archetype支持
    - [ ] 离线开发支持

## 发布历史
#### V0.0.1
- _Feature_ 框架基本结构
- _Feature_ 统一依赖定义

## 参与贡献
1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
