# 使用手册

## 一、简介

### 1.1 概述
- mat-framework是一个轻量级的分布式框架，内里整合了springboot、springcloud、gateway、skywalking等。
- mat-kernel-core是核心组件，在core里可以集成主要的模块功能，让各个模块后续基于core开发，可以只专注与业务逻辑层面的开发。

- 这么做有以下几个优点：
	1. 业务开发更专注，业务开发效率高
	2. 业务开发技术栈和开发方式统一，开发人员学习成本低，人员利用率高
    3. 框架可以单独维护、升级，框架升级对业务无影响
    4. 框架开发和业务开发职责明确，技术线和业务线分离


## 二、引包规范
- 统一在父工程中的pom文件，通过dependencyManagement统一管理
- 各依赖使用介绍
  1. mat-dependencies，是框架的组件库，版本统一在这里进行管理
    ```  
    <dependency>
        <groupId>org.mat-cloud.mat-framework</groupId>
        <artifactId>mat-dependencies</artifactId>
        <version>0.0.6-SNAPSHOT</version>
        <type>pom</type>
        <scope>import</scope>
    </dependency>
    ```
  2. 网关相关依赖，使用了springCloud的gateway功能
    ``` 
    <dependency>
        <groupId>org.mat-cloud.mat-framework</groupId>
        <artifactId>mat-kernel-api-gateway</artifactId>
    </dependency>
    ```
   3. 框架核心，集成springboot等，里面也集成了链路相关处理，可以基于它实现各个模块的主要功能
    ``` 
    <dependency>
        <groupId>org.mat-cloud.mat-framework</groupId>
        <artifactId>mat-kernel-core</artifactId>
    </dependency>
    ```   
  4. 语言扩展包
    ``` 
    <dependency>
            <groupId>org.mat-cloud.mat-framework</groupId>
            <artifactId>mat-kernel-lang</artifactId>
        </dependency>
    ```    
  5. 操作日志相关
  ``` 
     <dependency>
             <groupId>org.mat-cloud.mat-framework</groupId>
             <artifactId>mat-kernel-logging-operation</artifactId>
         </dependency>
     ```  
  6. 微服务能力增强，处理链路id传递等
  ``` 
     <dependency>
             <groupId>org.mat-cloud.mat-framework</groupId>
             <artifactId>mat-kernel-service</artifactId>
       </dependency>
    ``` 
  7. 工具类库
  ``` 
     <dependency>
             <groupId>org.mat-cloud.mat-framework</groupId>
             <artifactId>mat-kernel-util</artifactId>
       </dependency>
    ``` 
  8. web相关
  ``` 
     <dependency>
             <groupId>org.mat-cloud.mat-framework</groupId>
             <artifactId>mat-kernel-web</artifactId>
       </dependency>
    ``` 

## yml配置说明

> ```
> server:
>  port: 8081
>
>logging:     #日志相关配置
>  path: /var/log/demo-user-service/     
>  #  config: classpath:config/logback-spring.xml
>  level:
>    root: info
>    org.mat: debug
>  config: classpath:config/logback-spring.xml
>spring:
>  application:
>    name: mat-demo
>  profiles:
>    active: dev
>  main:
>    allow-bean-definition-overriding: true    #允许重名覆盖
>  http:    #http配置
>    encoding:
>      force: true
>      charset: UTF-8
>      enabled: true
>  cloud:
>    nacos:     #使用nacos,注册中心地址等配置
>      config:
>        group: public
>        server-addr: localhost
>        port: 8848
>        file-extension: yaml
>        timeout: 30000
>        refresh-enabled: true
>        max-retry: 10
>
>feign:
>  httpclient:
>    enabled: true
>    connection-timer-repeat: 3000 #重试时间
>    connection-timeout: 60000     #超时时间
>    max-connections: 200           #最大连接
>  okhttp:
>    enabled: false
>  #      ephemeral: false
>
>
>
>ribbon:
>  # read-timeout: 60000  #失效参数
>  # connect-timeout: 60000  #失效参数
>  ReadTimeout: 60000           #最大超时时间
>  ConnectTimeout: 60000
>
>management:
>  endpoint:
>    health:
>      show-details: always        #健康检查
> 
> 
> ```

## 工具类和语言扩展使用

### 1.1 主要工具类

-   ThreadLocal:线程相关使用
-   guava:引入了谷歌的guava工具类库
-   引入Lombok，让整体代码显得更加的简洁
-   commons-collections:集合相关工具类使用




						















