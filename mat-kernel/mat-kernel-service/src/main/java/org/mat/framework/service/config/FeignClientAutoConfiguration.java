package org.mat.framework.service.config;

import org.mat.framework.service.context.RequestContextFeignClientInterceptor;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: FeignClientAutoConfiguration
 * @Date: 2021/8/17
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
@Configuration
public class FeignClientAutoConfiguration {

    @Bean
    public RequestInterceptor requestContextFeignClientInterceptor(){
        return new RequestContextFeignClientInterceptor();
    }

}
