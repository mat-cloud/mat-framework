//package org.mat.framework.service.config;
//
//import com.netflix.hystrix.strategy.concurrency.HystrixConcurrencyStrategy;
//import org.mat.framework.service.context.RequestContextHystrixConcurrencyStrategy;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @ClassName: HystrixAutoConfiguration
// * @Date: 2021/8/18
// * @author: sunxinhe
// * @Version: 1.0
// * @Description: TODO
// */
//@Configuration
//public class HystrixAutoConfiguration {
//
//    @Bean
//    public HystrixConcurrencyStrategy requestContextHystrixConcurrencyStrategy(){
//        return new RequestContextHystrixConcurrencyStrategy();
//    }
//}
