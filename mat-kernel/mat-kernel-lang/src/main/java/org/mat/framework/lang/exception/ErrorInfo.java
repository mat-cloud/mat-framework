package org.mat.framework.lang.exception;

/**
 * <p>Title: Error</p>
 * <p>Date: 2019/6/27 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public interface ErrorInfo {

    /**
     * 获取状态码
     * TODO 状态码默认200，如何设计更合理
     * @return
     */
    int getStatus();

    /**
     * 获取错误编码
     *
     * @return
     */
    String getCode();

    /**
     * 获取错误信息
     *
     * @return
     */
    String getMsg();
}
