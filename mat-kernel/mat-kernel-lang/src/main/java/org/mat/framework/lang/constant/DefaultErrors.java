package org.mat.framework.lang.constant;


import org.mat.framework.lang.exception.ErrorInfo;

/**
 * <p>Title: ErrorCode</p>
 * <p>Date: 2019/6/27 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public enum DefaultErrors implements ErrorInfo {

    INVALID_PARAM_WITH_PLACEHOLDER("1003", "无效参数：{0}！"),
    INVALID_PARAM("1002", "无效参数！"),
    DEFAULT("1001", "系统内部错误！");

    private int status;

    private String code;

    private String msg;

    DefaultErrors(String code, String msg) {
        this.status = 200;
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

}
