package org.mat.framework.lang.dto;

import org.mat.framework.lang.exception.ErrorInfo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>Title: MatApiResponse</p>
 * <p>Date: 2019/6/27 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class MatApiResponse<T> {

    private String code;
    private String message;
    private String serverTime;
    private T data;

    public MatApiResponse() {

    }

    public MatApiResponse(ErrorInfo errorInfo) {
        this.setCode(errorInfo.getCode());
        this.setMessage(errorInfo.getMsg());
        this.setServerTime(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    }

    public MatApiResponse(String code, String message, String serverTime, T data) {
        this.code = code;
        this.message = message;
        this.serverTime = serverTime;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public MatApiResponse setCode(ErrorInfo errorInfo) {
        this.code = errorInfo.getCode();
        this.message = errorInfo.getMsg();
        return this;
    }

    public MatApiResponse setErrorInfo(ErrorInfo errorInfo) {
        this.code = errorInfo.getCode();
        this.message = errorInfo.getMsg();
        return this;
    }

    public MatApiResponse setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public MatApiResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getServerTime() {
        return serverTime;
    }

    public MatApiResponse setServerTime(String serverTime) {
        this.serverTime = serverTime;
        return this;
    }

    public T getData() {
        return data;
    }

    public MatApiResponse setData(T data) {
        this.data = data;
        return this;
    }


    public static <T> MatApiResponse<T> success(T data) {
        return new MatApiResponse()
                .setCode("200")
                .setServerTime(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .setMessage("Success")
                .setData(data);
    }

    public static MatApiResponse fail(String code, String message) {
        return new MatApiResponse()
                .setCode(code)
                .setServerTime(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .setMessage(message);
    }

}
