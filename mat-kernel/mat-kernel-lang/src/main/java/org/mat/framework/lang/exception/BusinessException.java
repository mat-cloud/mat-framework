package org.mat.framework.lang.exception;

import java.text.MessageFormat;

/**
 * <p>Title: BusinessException</p>
 * <p>Date: 2019/6/27 </p>
 * <p>Description: </p>
 * 业务异常类，根据业务断言产生的异常枚举信息，封装业务异常
 *
 * @author sunxinhe
 */
public class BusinessException extends RuntimeException {

    private int status;

    private String code;

    private String msg;

    private String errorCause = "unknown error cause!";

    private ErrorInfo errorInfo;

    public BusinessException(ErrorInfo errorInfo) {
        super(new RuntimeException(errorInfo.getMsg()));
        this.setErrorInfo(errorInfo);
        this.setStatus(errorInfo.getStatus());
        this.setCode(errorInfo.getCode());
        this.setMsg(errorInfo.getMsg());
        this.setErrorCause(errorCause);
    }

    public BusinessException(ErrorInfo errorInfo, String errorCause) {
        super(new RuntimeException(errorInfo.getMsg()));
        this.setErrorInfo(errorInfo);
        this.setStatus(errorInfo.getStatus());
        this.setCode(errorInfo.getCode());
        this.setMsg(errorInfo.getMsg());
        this.setErrorCause(errorCause);
    }

    public BusinessException(ErrorInfo errorInfo, Exception ex) {
        super(ex);
        this.setErrorInfo(errorInfo);
        this.setStatus(errorInfo.getStatus());
        this.setCode(errorInfo.getCode());
        this.setMsg(errorInfo.getMsg());
    }

    public BusinessException(ErrorInfo errorInfo, Exception ex, String errorCause) {
        super(ex);
        this.setErrorInfo(errorInfo);
        this.setStatus(errorInfo.getStatus());
        this.setCode(errorInfo.getCode());
        this.setMsg(errorInfo.getMsg());
        this.setErrorCause(errorCause);
    }

    public BusinessException(ErrorInfo errorInfo, String... args) {
        super(new RuntimeException(MessageFormat.format(errorInfo.getMsg(),args)));
        this.setErrorInfo(errorInfo);
        this.setStatus(errorInfo.getStatus());
        this.setCode(errorInfo.getCode());
        this.setMsg(MessageFormat.format(errorInfo.getMsg(),args));
        this.setErrorCause(errorCause);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErrorCause() {
        return errorCause;
    }

    public void setErrorCause(String errorCause) {
        this.errorCause = errorCause;
    }

    public ErrorInfo getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(ErrorInfo errorInfo) {
        this.errorInfo = errorInfo;
    }
}
