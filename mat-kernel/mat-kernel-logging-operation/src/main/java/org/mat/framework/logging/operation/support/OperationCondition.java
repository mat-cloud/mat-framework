package org.mat.framework.logging.operation.support;

import java.util.Map;

/**
 * <p>Title: OperationCondition </p>
 * <p>Date: 2020/4/27 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class OperationCondition {

    private Map<String, String[]> parameterMap;

    private String requestBody;

    public Map<String, String[]> getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(Map<String, String[]> parameterMap) {
        this.parameterMap = parameterMap;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }
}
