package org.mat.framework.logging.operation.api;

/**
 * <p>Title: OperateEnum </p>
 * <p>Date: 2020/4/22 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public enum OperateEnum {

    LOGIN("0", "登录"),
    QUERY("1", "查询"),
    INSERT("2", "新增"),
    UPDATE("3", "修改"),
    DELETE("4", "删除"),
    OTHER("5", "其他");


    private String operateType;
    private String operateName;

    private OperateEnum(String operateType, String operateName) {
        this.operateType = operateType;
        this.operateName = operateName;
    }

    public String getOperateType() {
        return this.operateType;
    }

    public String getOperateName() {
        return this.operateName;
    }
}
