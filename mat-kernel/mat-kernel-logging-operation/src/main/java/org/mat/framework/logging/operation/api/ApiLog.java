package org.mat.framework.logging.operation.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Title: ApiLog </p>
 * <p>Date: 2020/4/22 </p>
 * <p>Description: 操作日志记录注解</p>
 * <p>
 * 参数解释：
 * String paramExplain() default "";
 * 自定义匹配异常：
 * Class<? extends Throwable>[] matchException() default {};
 * 自定义异常错误码：
 * String[] exceptionCode() default {};
 * <p>
 * String jsonPathExplain() default "";
 *
 * @author sunxinhe
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiLog {

    /**
     * 操作类型
     * TODO 是不是可以直接传字符串呢？扩展操作的问题
     *
     * @return 操作类型枚举
     */
    OperateEnum operateType() default OperateEnum.OTHER;

    /**
     * 所属模块
     *
     * @return 所属模块名称
     */
    String operateModule() default "其他模块";

    /**
     * 是否记录请求结果
     *
     * @return 布尔值，默认false
     */
    boolean recordResult() default false;

}
