package org.mat.framework.logging.operation.support;

/**
 * <p>Title: DefaultOperationLoggingDTO </p>
 * <p>Date: 2020/4/22 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class DefaultOperationLoggingDTO {

    private String userId;
    private String operateTime;
    private String requestIp;
    private String operateType;
    private String operateName;
    private String operateModule;
    private String operateResult;
    private String errorCode;
    private String operateCondition;
    private Object returnResult;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(String operateTime) {
        this.operateTime = operateTime;
    }

    public String getRequestIp() {
        return requestIp;
    }

    public void setRequestIp(String requestIp) {
        this.requestIp = requestIp;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public String getOperateName() {
        return operateName;
    }

    public void setOperateName(String operateName) {
        this.operateName = operateName;
    }

    public String getOperateModule() {
        return operateModule;
    }

    public void setOperateModule(String operateModule) {
        this.operateModule = operateModule;
    }

    public String getOperateResult() {
        return operateResult;
    }

    public void setOperateResult(String operateResult) {
        this.operateResult = operateResult;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getOperateCondition() {
        return operateCondition;
    }

    public void setOperateCondition(String operateCondition) {
        this.operateCondition = operateCondition;
    }

    public Object getReturnResult() {
        return returnResult;
    }

    public void setReturnResult(Object returnResult) {
        this.returnResult = returnResult;
    }

    @Override
    public String toString() {
        return "DefaultOperationLoggingDTO{" +
                ", userId='" + userId + '\'' +
                ", operateTime='" + operateTime + '\'' +
                ", requestIp='" + requestIp + '\'' +
                ", operateType='" + operateType + '\'' +
                ", operateName='" + operateName + '\'' +
                ", operateModule='" + operateModule + '\'' +
                ", operateResult='" + operateResult + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", operateCondition='" + operateCondition + '\'' +
                ", returnResult=" + returnResult +
                '}';
    }
}
