package org.mat.framework.logging.operation.configure;


import org.mat.framework.logging.operation.support.DefaultOperationLoggingExecutor;
import org.mat.framework.logging.operation.support.OperationLoggingAspect;
import org.mat.framework.logging.operation.support.OperationLoggingExecutor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>Title: DefaultOperationLoggingAutoConfiguration </p>
 * <p>Date: 2020/4/22 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@Configuration
public class DefaultOperationLoggingAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public OperationLoggingExecutor operationLoggingExecutor() {
        return new DefaultOperationLoggingExecutor();
    }

    @Bean
    public OperationLoggingAspect operationLoggingAspect() {
        return new OperationLoggingAspect();
    }
}
