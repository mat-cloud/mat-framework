package org.mat.framework.logging.operation.support;


import org.mat.framework.logging.operation.api.ApiLog;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * <p>Title: OperationLoggingExecutor </p>
 * <p>Date: 2020/4/23 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public interface OperationLoggingExecutor {

    void saveLog(HttpServletRequest httpServletRequest, ApiLog apiLog, Object[] args, LocalDateTime beginTime, String errorCode);

}
