package org.mat.framework.api.gateway.config;

import org.mat.framework.api.gateway.context.TracingFilter;
import org.mat.framework.core.context.TraceIdAdaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: TracingAutoConfiguration
 * @Date: 2021/8/26
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
@Configuration
public class TracingAutoConfiguration {

    @Autowired
    private TraceIdAdaptor traceIdAdaptor;

    @Bean
    public TracingFilter traceIdFilter() {
        return new TracingFilter(traceIdAdaptor);
    }

}
