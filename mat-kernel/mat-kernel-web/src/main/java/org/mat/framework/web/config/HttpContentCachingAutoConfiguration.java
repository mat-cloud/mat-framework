//package org.mat.framework.web.config;
//
//import org.mat.framework.web.filter.HttpContentCachingFilter;
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * <p>Title: HttpContentCachingAutoConfiguration </p>
// * <p>Date: 2020/4/27 </p>
// * <p>Description: Http请求、响应Content（body）缓存自动配置</p>
// *
// * @author sunxinhe
// */
//@Configuration
//public class HttpContentCachingAutoConfiguration {
//
//    @Bean
//    public FilterRegistrationBean<HttpContentCachingFilter> httpContentCachingFilterFilterRegistrationBean() {
//        // 创建filter
//        HttpContentCachingFilter httpContentCachingFilter = new HttpContentCachingFilter();
//        // 注册过滤器
//        FilterRegistrationBean<HttpContentCachingFilter> registration = new FilterRegistrationBean<>(httpContentCachingFilter);
//        // 添加条件
//        registration.addUrlPatterns("/*");
//        return registration;
//    }
//}
