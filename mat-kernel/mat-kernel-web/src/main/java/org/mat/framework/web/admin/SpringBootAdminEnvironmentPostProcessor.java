package org.mat.framework.web.admin;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;

import java.util.Properties;

/**
 * @ClassName: SpringBootAdminEnvironmentPostProcessor
 * @Date: 2021/9/1
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 * management:
 * endpoints:
 * web:
 * exposure:
 * include: '*'
 * endpoint:
 * health:
 * show-details: always
 */
public class SpringBootAdminEnvironmentPostProcessor implements EnvironmentPostProcessor {

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        Properties props = new Properties();
        boolean needOverwriteProperties = false;

        String managementEndpointsWebExposureInclude = environment.getProperty("management.endpoints.web.exposure.include");
        if (StringUtils.isBlank(managementEndpointsWebExposureInclude)) {
            needOverwriteProperties = true;
            props.put("management.endpoints.web.exposure.include", "*");
        }

        String endpointHealthShowDetails = environment.getProperty("endpoint.health.show-details");
        if (StringUtils.isBlank(endpointHealthShowDetails)) {
            needOverwriteProperties = true;
            props.put("endpoint.health.show-details", "always");
        }

        if (needOverwriteProperties) {
            environment.getPropertySources().addFirst(new PropertiesPropertySource("mat_ext_spring_boot_admin_properties", props));
        }

    }
}
