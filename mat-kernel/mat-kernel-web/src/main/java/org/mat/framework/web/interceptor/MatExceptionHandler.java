package org.mat.framework.web.interceptor;

import org.mat.framework.lang.constant.DefaultErrors;
import org.mat.framework.lang.dto.MatApiResponse;
import org.mat.framework.lang.exception.BusinessException;
import org.mat.framework.lang.exception.ErrorInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Title: MatExceptionHandler </p>
 * <p>Date: 2020/4/13 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@Slf4j
@RestControllerAdvice
public class MatExceptionHandler {

    @ExceptionHandler(value = BusinessException.class)
    public MatApiResponse bussinessExceptionHandler(HttpServletRequest request, BusinessException exception) {
        log.error("business exception: ",exception);
        ErrorInfo errorInfo = exception.getErrorInfo();
        MatApiResponse matApiResponse = new MatApiResponse(errorInfo);
        return matApiResponse;
    }


    @ExceptionHandler(value = RuntimeException.class)
    public MatApiResponse runtimeExceptionHandler(HttpServletRequest request, RuntimeException exception) {
        log.error("system runtime exception: ",exception);
        MatApiResponse matApiResponse = new MatApiResponse();
        matApiResponse.setCode(DefaultErrors.DEFAULT.getCode());
        matApiResponse.setMessage(exception.getMessage());
        return matApiResponse;
    }
}
