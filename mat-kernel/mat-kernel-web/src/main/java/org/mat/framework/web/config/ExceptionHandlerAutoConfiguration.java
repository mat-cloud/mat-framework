package org.mat.framework.web.config;

import org.mat.framework.web.interceptor.MatExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>Title: ExceptionHandlerAutoConfiguration </p>
 * <p>Date: 2020/4/13 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@Configuration
public class ExceptionHandlerAutoConfiguration {
    
    @Bean
    public MatExceptionHandler matExceptionHandler(){
        return new MatExceptionHandler();
    }
}
