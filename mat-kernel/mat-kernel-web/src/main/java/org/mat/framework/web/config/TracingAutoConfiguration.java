package org.mat.framework.web.config;

import org.mat.framework.core.context.TraceIdAdaptor;
import org.mat.framework.web.interceptor.RequestTracingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p>Title: TracingAutoConfiguration</p>
 * <p>Date: 2019-06-28 </p>
 * <p>Description: 链路跟踪自动配置</p>
 *
 * @author sunxinhe
 */
@Configuration
public class TracingAutoConfiguration implements WebMvcConfigurer {

    @Autowired
    private TraceIdAdaptor traceIdAdaptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        RequestTracingInterceptor requestTracingInterceptor = new RequestTracingInterceptor(traceIdAdaptor);
        registry.addInterceptor(requestTracingInterceptor)
                .addPathPatterns("/**");
    }
}


