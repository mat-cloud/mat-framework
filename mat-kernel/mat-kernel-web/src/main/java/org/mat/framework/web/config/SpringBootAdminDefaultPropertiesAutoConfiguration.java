//package org.mat.framework.web.config;
//
//import org.mat.framework.web.admin.SpringBootAdminEnvironmentPostProcessor;
//import org.springframework.boot.env.EnvironmentPostProcessor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @ClassName: SpringBootAdminDefaultPropertiesAutoConfiguration
// * @Date: 2021/9/1
// * @author: sunxinhe
// * @Version: 1.0
// * @Description: TODO
// */
//@Configuration
//public class SpringBootAdminDefaultPropertiesAutoConfiguration {
//
//    @Bean
//    public EnvironmentPostProcessor springBootAdminEnvironmentPostProcessor(){
//        return new SpringBootAdminEnvironmentPostProcessor();
//    }
//
//}
