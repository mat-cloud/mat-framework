package org.mat.framework.core.context;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

/**
 * @ClassName: RequestContext
 * @Date: 2021/8/17
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
@Data
@Builder
public class RequestContext {

    private String traceId;

    /**
     * TODO 标记是否Debug模式
     */
    private boolean debugEnabled = false;

    private long startTime;

    private long endTime = -1;

    private Map<String, String> data;

}
