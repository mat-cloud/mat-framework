package org.mat.framework.core.context;

/**
 * @ClassName: TraceIdGenerator
 * @Date: 2021/8/18
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
public interface TraceIdAdaptor {
     String getTraceId();

}
