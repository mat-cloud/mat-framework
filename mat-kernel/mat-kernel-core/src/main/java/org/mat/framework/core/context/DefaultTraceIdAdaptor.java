package org.mat.framework.core.context;

/**
 * @ClassName: DefaultTraceIdAdaptor
 * @Date: 2021/8/18
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
public class DefaultTraceIdAdaptor implements TraceIdAdaptor {

    @Override
    public String getTraceId() {
        return TraceIdGenerator.generate();
    }

}
