package org.mat.framework.core.context;

/**
 * @ClassName: RequestContextConstants
 * @Date: 2021/8/17
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
public class RequestContextConstants {

    /**
     * traceId key（用于SLF4J MDC）
     */
    public static String TRACE_ID_KEY_FOR_MDC = "traceId";

    /**
     * traceId key（用于HTTP请求（响应）头信息）
     */
    public static String TRACE_ID_KEY_FOR_HTTP_HEADER = "x-trace-id";

}
