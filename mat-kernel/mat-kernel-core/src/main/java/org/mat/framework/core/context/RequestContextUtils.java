package org.mat.framework.core.context;

import java.util.EmptyStackException;

/**
 * @ClassName: RequestContextUtils
 * @Date: 2021/8/17
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
public class RequestContextUtils {

    private static final ThreadLocal<RequestContext> threadLocal = new ThreadLocal<RequestContext>();

    public static void set(RequestContext requestContext) {
        if (requestContext == null) {
            return;
        }
        threadLocal.set(requestContext);
    }

    public static RequestContext get() throws EmptyStackException {
        return threadLocal.get();
    }

    public static String getTraceId() throws EmptyStackException {
        String traceId = null;
        RequestContext requestContext = threadLocal.get();
        if (requestContext != null) {
            traceId = requestContext.getTraceId();
        }
        return traceId;
    }

    public static RequestContext getAndClear() throws EmptyStackException {
        RequestContext requestContext = threadLocal.get();
        clear();
        return requestContext;
    }

    public static void clear() {
        threadLocal.remove();
    }

    public static RequestContext init(String traceId) {
        RequestContext requestContext;
        if (get() != null) {
            requestContext = get();
            requestContext.setTraceId(traceId);
        } else {
            requestContext = RequestContext.builder()
                    .traceId(traceId)
                    .startTime(System.currentTimeMillis())
                    .build();
        }
        set(requestContext);

        return requestContext;
    }
}
