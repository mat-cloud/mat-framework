//package org.mat.framework.core.configure;
//
//import org.slf4j.MDC;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.task.TaskDecorator;
//import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
//import org.springframework.scheduling.annotation.EnableAsync;
//import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
//
//import java.util.Map;
//import java.util.concurrent.Executor;
//
///**
// * @ClassName: RequestContextUtils
// * @Date: 2021/8/17
// * @author: sunxinhe
// * @Version: 1.0
// * @Description: TODO 还有bug，修复后再提供该特性
// */
////@Configuration
////@EnableAsync
//public class LogbackThreadContextAutoConfiguration extends AsyncConfigurerSupport {
//
//    @Override
//    public Executor getAsyncExecutor() {
//        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
//        executor.setTaskDecorator(new MdcTaskDecorator());
//        executor.initialize();
//        return executor;
//    }
//
//    class MdcTaskDecorator implements TaskDecorator {
//
//        @Override
//        public Runnable decorate(Runnable runnable) {
//            // Right now: Web thread context !
//            // (Grab the current thread MDC data)
//            Map<String, String> contextMap = MDC.getCopyOfContextMap();
//            return () -> {
//                try {
//                    // Right now: @Async thread context !
//                    // (Restore the Web thread context's MDC data)
//                    MDC.setContextMap(contextMap);
//                    runnable.run();
//                } finally {
//                    MDC.clear();
//                }
//            };
//        }
//    }
//
//}
