package org.mat.framework.core.configure;

import org.mat.framework.core.context.DefaultTraceIdAdaptor;
import org.mat.framework.core.context.TraceIdAdaptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: DefaultTraceIdAdaptorAutoConfiguration
 * @Date: 2021/8/18
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 * 配置默认的TraceId生成器，整合三方链路追踪工具时可以覆盖 DefaultTraceIdAdaptor
 */
@Configuration
public class DefaultTraceIdAdaptorAutoConfiguration {

    @ConditionalOnMissingBean({TraceIdAdaptor.class})
    @Bean
    public TraceIdAdaptor traceIdAdaptor() {
        return new DefaultTraceIdAdaptor();
    }

}
