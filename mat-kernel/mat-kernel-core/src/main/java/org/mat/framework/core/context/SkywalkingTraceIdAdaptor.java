package org.mat.framework.core.context;

import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.TraceContext;

/**
 * @ClassName: SkywalkingTraceIdGenerator
 * @Date: 2021/8/18
 * @author: sunxinhe
 * @Version: 1.0
 * @Description: TODO
 */
@Slf4j
public class SkywalkingTraceIdAdaptor implements TraceIdAdaptor {

    @Override
    public String getTraceId() {
        String traceId = TraceContext.traceId();
        log.debug("从skywalking获取traceId : {}", traceId);
        return traceId;
    }

}
