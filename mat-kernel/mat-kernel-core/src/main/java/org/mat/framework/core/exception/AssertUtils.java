package org.mat.framework.core.exception;

import org.mat.framework.lang.exception.BusinessException;
import org.mat.framework.lang.exception.ErrorInfo;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>Title: AssertUtils</p>
 * <p>Date: 2019-06-28 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class AssertUtils {

    private static void throwIt(ErrorInfo err) {
        throw new BusinessException(err);
    }


    public static void assertEmpty(String str, ErrorInfo err) {
        if (StringUtils.isNotEmpty(str)) {
            throwIt(err);
        }
    }

    public static void assertBlank(String str, ErrorInfo err) {
        if (StringUtils.isNotBlank(str)) {
            throwIt(err);
        }
    }

    public static void assertNotEmpty(String str, ErrorInfo err) {
        if (StringUtils.isEmpty(str)) {
            throwIt(err);
        }
    }

    public static void assertNotBlank(String str, ErrorInfo err) {
        if (StringUtils.isBlank(str)) {
            throwIt(err);
        }
    }

    public static void assertTrue(boolean flag, ErrorInfo err, String... args) {
        if (!flag) {
            throwIt(err,args);
        }
    }

    private static void throwIt(ErrorInfo err, String... args) {
        throw new BusinessException(err,args);
    }

    public static void assertTrue(boolean flag, ErrorInfo err) {
        if (!flag) {
            throwIt(err);
        }
    }

    public static void assertFalse(boolean flag, ErrorInfo err) {
        if (flag) {
            throwIt(err);
        }
    }

    public static void assertNull(Object object, ErrorInfo err) {
        if (object != null) {
            throwIt(err);
        }
    }

    public static void assertNotNull(Object object, ErrorInfo err) {
        if (object == null) {
            throwIt(err);
        }
    }

    public static void assertNumber(String str, ErrorInfo err) {
        if (StringUtils.isNumeric(str)) {
            throwIt(err);
        }
    }
}
